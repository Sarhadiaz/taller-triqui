class Triqui {

    constructor() {

        this.dibujarTablero();
        this.definirJugador();
    }

    dibujarTablero() {

        //Crear un objeto que referencie a la división tablero
        let miTablero = document.getElementById("tablero");

        for (let i = 0; i < 9; i++) {
            miTablero.innerHTML = miTablero.innerHTML + 
            "<input type='button' id='casilla"+(i+1)+"' class='casilla' onclick='miTriqui.hacerjugada()'/>"

            if((i+1)%3==0){
                miTablero.innerHTML = miTablero.innerHTML + "<br>";
            }

        }
    }

    definirJugador(){
        let n;

        let miTurno = document.getElementById("turno");
        n=Math.round(Math.random()+1);

        if(n===1){
            this.turno="X";
            
        }else{
            this.turno="O";
        }
        miTurno.innerHTML="Es el turno de: "+ this.turno;   
             
    }
    hacerjugada()
    {
        let i=event.target;
        i.value=this.turno;
    
    }
    

}